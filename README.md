# OpenML dataset: Winedata

https://www.openml.org/d/43651

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Thinking of Natural Language Processing as a beginner!!
The dataset has been about the wine comments or reviews that has been given by various wine tasters. The concept was to use text classification to classify the commenters over the reviews. This is mainly used to demonstrate the techniques involved in language processing. The assumption is that every taster has some specific style of describing their object of interest.
Content
The columns contain the reviews, country and provinces of wines, their variety and winery they belong to along with commenters or tasters.
Acknowledgements
The kaggle dataset for wine data with comments, the courses regarding the NLP had been very much helpful for understanding and implementation of the concept.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43651) of an [OpenML dataset](https://www.openml.org/d/43651). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43651/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43651/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43651/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

